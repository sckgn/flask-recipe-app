from project import db, app
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from time import time
import jwt
from werkzeug.security import generate_password_hash, check_password_hash

link = db.Table('link', db.Model.metadata,
    db.Column('recipeId', db.Integer, db.ForeignKey('recipe.id')),
    db.Column('categoryId', db.Integer, db.ForeignKey('category.id'))
)

#one-to-many with recipes
class User (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(250), index=True)
    surname = db.Column(db.String(250), index=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    authenticated = db.Column(db.Boolean, default=False)
    # recipe_id = db.Column(db.Integer,db.ForeignKey('recipe.id'))
    recipes = db.relationship('Recipe', backref='user',lazy=True)
    
    # def __init__(self, fistname, surname, email, password):
    #     self.firstname = firstname 
    #     self.surname = surname
    #     self.email = email
    #     self.password = password
    #     self.authenticated = False
    
    # hybrid properties and functions required for configuration of Flask-Login

    # check if the provided password is correct
    @hybrid_method
    def is_correct(self, password):
        return self.password == password
    
    # returns True if user is authenticated 
    @property
    def is_authenticated(self):
        return self.authenticated  

    # always True
    @property
    def is_active(self):
        return True

    # anonymous users are not permitted 
    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return self.firstname + ' ' + self.surname

    # token generation and verification functions for password reset
    # token is valid for 10 minutes
    def get_res_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256'
        )
    
    @staticmethod
    def verify_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


#one-to-many with user (one user, many recipes)
#many-to-many with categories
#one-to-one with image
class Recipe (db.Model):
    __tablename__ = 'recipe'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), index=True)
    servings = db.Column(db.Integer)
    cooking_time = db.Column(db.Integer)
    difficulty = db.Column(db.String(20), index=True)
    calories = db.Column(db.Integer)
    ingredients = db.Column(db.Text, nullable=False)
    method = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'))
    categories = db.relationship('Category',secondary=link)
    # image = db.Column(db.Integer,db.ForeignKey('image.id'))

    def __repr__(self):
        return self.title  

#one-to-one with recipes (every recipes can have one image,an image can be associated only with one recipe)
class Image (db.Model):
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key=True)
    img = db.Column(db.Text, unique=True)
    name = db.Column(db.Text, index=True)
    mimetype = db.Column(db.Text, index=True)
    # recipe = db.relationship('Recipe',backref="image",lazy='dynamic')

    def __repr__(self):
        return self.name + '.' + self.mimetype

#many-to-many with recipes (a recipe belongs in many categories, one category can be assigned to many recipes)
class Category (db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    dish_type =  db.Column(db.Text, index=True)
    recipes = db.relationship('Recipe', secondary=link)

    def __repr__(self):
        return self.dish_type