from flask import Blueprint, render_template, flash, redirect, session, url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_login import login_user, current_user, login_required, logout_user
from flask_mail import Message

from sqlalchemy import exc

from project import app, db ,admin, mail
from project.models import User, Recipe, Image, Category
from project.forms import UserSignupForm, LoginUserForm, ResetPasswordReqForm, ResetPasswordForm, NewPasswordForm

admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Recipe, db.session))
admin.add_view(ModelView(Image, db.session))
admin.add_view(ModelView(Category, db.session))

users_bp = Blueprint('users',__name__)

# helper functions
def send_email(subject, sender, recipients, text_body, html_body):
    message = Message(subject, sender=sender, recipients=recipients)
    message.body = text_body
    message.html = html_body
    mail.send(message)

def send_password_res_email(user):
    token = user.get_res_password_token()
    send_email('[Recipe Diary] Reset Your Password',
                sender=app.config['ADMINS'][0],
                recipients=[user.email],
                text_body=render_template('email/res_password.txt',
                                         user=user, token=token),
                html_body=render_template('email/res_password.html',
                                         user=user, token=token)
    )

# user routes
@users_bp.route('/signup',methods=['GET','POST'])
def sign_up():
    form = UserSignupForm()
    # flash('Errors="%s"'%(form.errors),"warning")
    if form.validate_on_submit():
        try:
            u = User(firstname=form.firstname.data, surname = form.surname.data, email = form.email.data)
            u.set_password(form.password.data)
            u.authenticated = True
            db.session.add(u)
            db.session.commit()

            message = Message(subject='[Recipe Diary] Registration',
                              body='Thank you for registering!',
                              recipients=[u.email])
            mail.send(message)
            flash("Thank you for signing up!","success")
            return redirect('/login')
        except exc.IntegrityError:
            db.session.rollback()
            flash("Error! Email ({}) already exists.".format(form.email.data),"danger")    
        return redirect('/signup')
    
    return render_template('sign_up.html',
                            title="Sign Up",
                            form = form)

@users_bp.route('/login',methods=['GET','POST'])
def login():
    form = LoginUserForm()
    if form.validate_on_submit():
        u = User.query.filter_by(email=form.email.data).first()
        if u is not None and u.check_password(form.password.data):
            u.authenticated = True
            db.session.add(u)
            db.session.commit()
            login_user(u)
            flash("Welcome back, {}!".format(current_user.firstname),"success")
            return redirect('/')
        else:
            flash("Please check your login details and try again","danger")
            return redirect('/login')

    return render_template('login.html',
                            title="Login",
                            form=form)

@users_bp.route('/logout')
@login_required
def logout():
    u = current_user
    u.authenticated = False
    db.session.add(u)
    db.session.commit()
    logout_user()
    flash("Goodbye!", "info")
    return redirect('/login')

@users_bp.route('/reset_password_req',methods=['GET','POST'])
def reset_password_req():
    if current_user.is_authenticated:
        return redirect('/')
    form = ResetPasswordReqForm()
    if form.validate_on_submit():
        u = User.query.filter_by(email=form.email.data).first()
        if u:
            send_password_res_email(u)
        flash('Check your email for password reset instructions','info')
        return redirect('/login')
    return render_template('reset_password_req.html',
                            title='Reset Password',
                            form=form)
    
@users_bp.route('/reset_password/<token>', methods=['GET','POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect('/')
    u = User.verify_token(token)
    if not u:
        return redirect('/')
    form = ResetPasswordForm()
    if form.validate_on_submit():
        u.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.','info')
        return redirect('/login')
    return render_template('reset_password.html', form=form)

@users_bp.route('/profile',methods=['GET'])
def profile():
    if current_user.is_authenticated:
        return render_template('user_profile.html',
                                title='My Profile')

@users_bp.route('/change_password', methods=["GET","POST"])
@login_required
def change_password():
    form = NewPasswordForm()
    if form.validate_on_submit():
        user = current_user
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Password has been updated successfuly!', 'success')
        return redirect('/profile')
    return render_template('change_password.html', form=form)
