from flask import render_template, Blueprint, flash, redirect, session, url_for, request

from flask_login import login_user, current_user, login_required, logout_user
from project import app, db ,admin, mail
from project.models import User, Recipe, Image, Category
from project.forms import NewRecipeForm,NewCategoryForm


recipes_bp = Blueprint('recipes',__name__)

@recipes_bp.route('/')
def index():
    return render_template('home.html')

@recipes_bp.route('/add',methods=['GET','POST'])
def add():
    form = NewRecipeForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            recipe = Recipe(title=form.title.data,servings=form.servings.data,
                        cooking_time=form.cooking_time.data, difficulty=form.difficulty.data,
                        calories=form.calories.data, ingredients=form.ingredients.data,
                        method=form.method.data)
            for catid in form.categories.data:
                category = Category.query.get(catid)
                recipe.categories.append(category)
            user = User.query.get(form.user.data)
            recipe.user_id=user.id
            db.session.add(recipe)
            db.session.commit()
            flash('A new recipe has been added succesfuly!','success')
            return redirect('/recipes')
        else:
            flash('Error!Recipe was not added!',"danger")
            return redirect('/add')
    return render_template('add_recipe.html',
                            form = form)

@recipes_bp.route('/recipes',methods=['GET'])
def allRecipes():
    recipes=Recipe.query.all()
    return render_template('all_recipes.html',
                            title='All Recipes',
                            recipes=recipes)

@recipes_bp.route('/categories',methods=['GET'])
def categories():
    categories=Category.query.all()
    return render_template('categories.html',
                            title="All Categories",
                            categories=categories)


@recipes_bp.route('/categories/<id>',methods=['GET'])
def getRecipesByCategory(id):
    c = Category.query.filter_by(id=id).first()
    return render_template('categories.html',
                            title='Recipes in the category ' + str(c),
                            categories=c.recipes)

@recipes_bp.route('/add_category',methods=['GET','POST'])
def addCategory():
    form=NewCategoryForm()
    if form.validate_on_submit():
        if current_user.is_authenticated:
            category=Category(dish_type=form.dish_type.data)
            for recid in form.recipes.data:
                recipe = Recipe.query.get(recid)
                category.recipes.append(recipe)
            db.session.add(category)
            db.session.commit()
            flash('You have added a new category!','success')
            return redirect('/categories')
        else:
            flash('Error adding category!','danger')
            return redirect('/add_category')
    return render_template('add_category.html',
                            title='New Category',
                            form=form)
