from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField, BooleanField, TextAreaField, SelectMultipleField, SelectField, PasswordField, SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Email, EqualTo, Length, Optional, NumberRange
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flask_login import login_user, current_user, login_required, logout_user
from .models import User, Recipe, Image, Category

class UserSignupForm(FlaskForm):
    firstname = StringField('First name', validators=[DataRequired()])
    surname = StringField('Surname', validators=[DataRequired()])
    email = StringField (
        'Email',
        validators=[
            Length(min=7),
            Email(message='Enter a valid email.'),
            DataRequired()
        ]
    )
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=6, message='Select a stronger password.')

        ]
    )
    confirm = PasswordField(
        'Confirm your password',
        validators =[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')

        ]
    )
    submit = SubmitField('Register')


class LoginUserForm(FlaskForm):
    email = StringField (
        'Email',
        validators=[
            DataRequired(),
            Email(),
            Length(min=6, max=40)
        ]
    )
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
        ]
    )
    submit = SubmitField('Login')

class ResetPasswordReqForm(FlaskForm):
    email = StringField (
        'Email',
        validators=[
            DataRequired(),
            Email(),
            Length(min=6, max=40)
        ]
    )
    submit = SubmitField('Request Password Reset')

class ResetPasswordForm(FlaskForm):
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=6, message='Select a stronger password.')
        ]
    )
    confirm = PasswordField(
        'Confirm your password',
        validators =[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')

        ]
    )
    submit = SubmitField('Reset Password')

class NewRecipeForm(FlaskForm):
    title = StringField(
        'Title',
        validators = [
            DataRequired()
        ]
    )
    servings = SelectField(
        'Servings',
         choices=[('1'),('2'),('3'),('>5'),('>10')],
         validators=[
             Optional()
         ]
    )
    cooking_time = IntegerField(
        'Cooking Time in Minutes',
        validators=[
             Optional()
         ]
    )
    difficulty = SelectField(
        'Difficulty',
        choices=[('easy'),('advanced'),('chef')],
        validators=[
             Optional()
         ]
    )
    calories = IntegerField(
        'Calories per porion',
        validators=[
             Optional(),
             NumberRange(min=1, max=1000)
         ]
    )
    ingredients = TextAreaField(
        'Ingredients',
        validators=[
            DataRequired()
        ]
    )
    method = TextAreaField(
        'Preparation',
        validators=[
            DataRequired()
        ]
    )
    users = [(u.id, u.firstname+ " "+u.surname) for u in User.query.order_by('surname')]
    user = SelectField('user', coerce=int,choices=users)
    choices = [(c.id, c.dish_type) for c in Category.query.order_by('dish_type')]
    categories = SelectMultipleField('Categories',coerce=int, choices = choices)
    submit = SubmitField('Add Recipe')

class NewCategoryForm(FlaskForm):
    dish_type = StringField (
        'Category',
        validators=[
            DataRequired()
        ]
    )
    choices=[(r.id,r.title) for r in Recipe.query.order_by('title')]
    recipes = SelectMultipleField('Recipes',coerce=int,choices=choices)
    submit= SubmitField('Add Category') 

class NewPasswordForm(FlaskForm):
    password = PasswordField(
        'New Password',
        validators=[
            DataRequired()
        ]
    )
    confirm = PasswordField(
        'Confirm your new password',
        validators =[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')

        ]
    )
    submit = SubmitField('Update Password')