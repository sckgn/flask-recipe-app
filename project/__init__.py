from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate 
from flask_admin import Admin
from flask_login import LoginManager
from flask_mail import Mail



app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
mail = Mail(app)


admin = Admin(app, template_mode='bootstrap3')
migrate = Migrate(app,db,render_as_batch=True)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'users.login'

from project.models import User

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == int(user_id)).first()

from project.recipes.views import recipes_bp
from project.users.views import users_bp

app.register_blueprint(recipes_bp)
app.register_blueprint(users_bp)

from project import models
from project.recipes import views
from project.users import views 

