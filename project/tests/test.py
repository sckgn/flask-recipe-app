import os 
import unittest
from flask import Flask 
from flask_sqlalchemy import SQLAlchemy


from project import app, db, models 

TEST_DB = 'test.db'
basedir = os.path.abspath(os.path.dirname(__file__))

class BasicTests(unittest.TestCase):
    # Set Up and Tear Down

    #execute prior each test
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        # app.config('DEBUG') = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
            os.path.join(basedir, TEST_DB)
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
    # execute after each test
    def tearDown(self):
        # db.session.remove()
        # db.drop_all()
        pass

    # helper functions
    def signup(self, firstname, surname, email, password, confirm):
        return self.app.post(
            '/signup',
            data=dict(firstname=firstname, surname=surname, email=email, password=password, confirm=confirm),
            follow_redirects=True
        )
    def login(self, email, password):
        return self.app.post(
            '/login',
            data=dict(email=email, password=password),
            follow_redirects=True
        )
    def add(self, title, servings, cooking_time, difficulty, calories, ingredients, method, user_id, categories):
        return self.app.post(
            data=dict(title=title,servings=servings,cooking_time=cooking_time,difficulty=difficulty,calories=calories,ingredients=ingredients,method=method,user_id=user_id,categories=categories),
            follow_redirects=True
        )
    # tests
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code,200)

    # user tests
    def test_user_signup_form_appears(self):
        response = self.app.get('/signup',follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Sign up',response.data)
    
    def test_valid_user_signup(self):
        self.app.get('/signup', follow_redirects=True)
        response = self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.assertIn(b'Thank you for signing up!',response.data)

    def test_duplicate_email_error(self):
        self.app.get('/signup', follow_redirects=True)
        self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.app.get('/signup', follow_redirects=True)
        response = self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.assertIn(b'Error! Email (pp@mail.com) already exists.',response.data)
    
    def test_login_form_appears(self):
        response = self.app.get('/login',follow_redirects=True)
        self.assertEquals(response.status_code, 200)
        self.assertIn(b'Login',response.data)

    def test_valid_login(self):
        self.app.get('/signup', follow_redirects=True)
        self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.app.get('/login',follow_redirects=True)
        response = self.login('pp@mail.com','spiderman1')
        self.assertIn(b'Welcome back, Peter!', response.data)
    
    def test_invalid_login(self):
        self.app.get('/login',follow_redirects=True)
        response = self.login('pp@mail.com','spiderman1')
        self.assertIn(b'Please check your login details and try again', response.data)

    def test_logout(self):
        self.app.get('/signup',follow_redirects=True)
        self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.app.get('/login', follow_redirects=True)
        self.login('pp@mail.com','spiderman1')
        response = self.app.get('/logout',follow_redirects=True)
        self.assertIn(b'Goodbye!',response.data)

    def test_user_profile(self):
        self.app.get('/signup',follow_redirects=True)
        self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.app.get('/login', follow_redirects=True)
        self.login('pp@mail.com','spiderman1')
        response = self.app.get('/profile',follow_redirects=True)
        self.assertIn(b'My Profile',response.data)


    # recipe tests
    def test_add_recipe_form_appears(self):
        response = self.app.get('/add',follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Add A New Recipe',response.data)

    def test_invalid_recipe_add(self):
        self.app.get('/signup',follow_redirects=True)
        self.signup('Peter','Parker','pp@mail.com','spiderman1','spiderman1')
        self.app.get('/login', follow_redirects=True)
        self.login('pp@mail.com','spiderman1')
        self.app.get('/add',follow_redirects=True)
        response=self.add('Recipe',3,20,'easy',300,'ingredients','method','Peter Parker','vegan')
        self.assertIn(b'Method Not Allowed',response.data)

    def test_display_all_recipes(self):
        response = self.app.get('/recipes',follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'All Recipes',response.data)




if __name__=='__main__':
    unitest.main()
